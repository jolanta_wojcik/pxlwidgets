const findAnagrams = (elements) => {
    let temp = "";
    let dict = {};
    let finalResult = [];
    let result = [];
    for (const element of elements) {
        temp = element;
        let sorted = element.replace(/ /g,'').split('').sort().join('');
        if(sorted in dict){
            dict[sorted].push(temp);
        }else{
            dict[sorted] = [temp];
        }
    }
    for(const [key, value] of Object.entries(dict)) {
        finalResult.push(value);
    }
    return finalResult;
};

console.log(findAnagrams([
        'rope',
        'pore',
        'repo',
        'red rum',
        'murder',
        'listen',
        'silent',
        'endeavour',
    ]
))
