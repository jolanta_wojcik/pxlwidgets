import './App.css';
import axios from "axios";
import React, {useCallback} from "react";
import ArtObject from "./ArtObject/ArtObject";
import ArtObjectDetails from "./ArtObjectDetailView/ArtObjectDetails";
import {Route, Switch, BrowserRouter, useHistory} from "react-router-dom";


// const axios = require('axios');
// https://www.rijksmuseum.nl/api/nl/collection?key=uDoXheEq

const {useEffect, useState} = React;

const fetchData = () => {
    // Make a request for a user with a given ID
    return axios
        .get('https://www.rijksmuseum.nl/api/nl/collection?key=uDoXheEq')
        .then(({data}) => {
            return data;
        })
        .catch(error => {
            // handle error
            console.error(error);
        });
}

function App() {
    const [artObjects, setArtObjects] = useState([]);
    const history = useHistory();
    const selectedHandler = objectNumber => {
        let path = '/' + objectNumber;
        history.push(path);
    }

    useEffect(() => {
        fetchData().then(data => {
            setArtObjects(data.artObjects)
        })
    }, []);

    let paintings = artObjects.map(object => (
        <ArtObject
            key={object.id}
            title={object.title}
            author={object.principalOrFirstMaker}
            image={object.webImage.url}
            clicked={objectNumber => {
                selectedHandler(object.objectNumber);
            }}
        />
    ));

    return (
        <div>
            <p className="App-search">Search content </p>
            <input className="App-search"></input>
            <section className="App-section">{paintings}</section>
        </div>
    );
}

export default App;
