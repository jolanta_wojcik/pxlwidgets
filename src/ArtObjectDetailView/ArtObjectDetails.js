import React, {Component} from "react";
import axios from "axios";

class ArtObjectDetails extends Component {
    state = {
        loadedDetailData: null
    };

    componentDidMount() {
        console.log("from fullpost", this.props);
        this.fetchDetailData();
    }

    componentDidUpdate() {
        this.fetchDetailData();
    }

    fetchDetailData () {
        return axios
            .get('https://www.rijksmuseum.nl/api/nl/collection/'+this.props.match.params.objectNumber+'?key=uDoXheEq')
            .then(response => {
                console.log(response)
                this.setState({
                    loadedDetailData: response.data
                });
            });
    }

    render() {
        let detailView = (
            <div >
                <h1>{this.state.loadedDetailData.title}</h1>
                <p>{this.state.loadedDetailData.description}</p>
                <img src={this.state.loadedDetailData.webImage.url}></img>
            </div>
        );

        return detailView;
    }
}

export default ArtObjectDetails;
