import React from "react";
import "./ArtObject.css";

const artObject = props => (
    <article className="ArtObject" onClick={props.clicked}>
        <img className="Image" src={props.image}></img>
        <h1>{props.title}</h1>
        <div className="Author">{props.author}</div>
    </article>
);

export default artObject;
